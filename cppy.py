#!/usr/bin/env python3
#
# ---------------------
# The MIT License (MIT)
#
# Copyright (c) 2013 Francis Hart
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ---------------------
#
# Written by Francis Hart
# http://hartcw.com
# http://github.com/hartcw/cppy

import io
import os
import re
import sys
import optparse
import platform
import builtins

class context:
    indent     = ''
    srcdir     = ''
    dstdir     = ''
    buffer     = None
    first      = True
    variables  = dict()
    colour_log = platform.system() != 'Windows'
    debug      = False

def parse_options():
    parser = optparse.OptionParser('cppy')
    parser.add_option('-i', '--input',
                      default='',
                      help='Specify the input file (defaults to stdin)')
    parser.add_option('-o', '--output',
                      default='',
                      help='Specify the output file (defaults to stdout)')
    parser.add_option('-t', '--type',
                      default='auto',
                      choices=['c', 'xml', 'auto'],
                      help='Set the type of embedded comment to match and expand (defaults to auto)')
    parser.add_option('-d', '--debug',
                      default=False,
                      action='store_true',
                      help='Enable debug information')
    (options, args) = parser.parse_args()
    return options

def log_set_colour(code):
    if context.colour_log:
        sys.stdout.write('\033[{};1m'.format(str(code)))

def log_unset_colour():
    if context.colour_log:
        sys.stdout.write('\033[0m')
        sys.stdout.flush()

def log_error(message):
    log_set_colour(31)
    sys.stdout.write('ERROR: {}\n'.format(message))
    log_unset_colour()
    exit()

def log_warning(message):
    log_set_colour(33)
    sys.stdout.write('WARNING: {}\n'.format(message))
    log_unset_colour()

def log_message(message):
    log_set_colour(32)
    sys.stdout.write(message)
    log_unset_colour()

def log_debug(message):
    if context.debug:
        log_set_colour(32)
        sys.stdout.write('DEBUG: {}\n'.format(message))
        log_unset_colour()

def print(*objects, sep=' ', end='\n', file=None, flush=False):
    if file == None:
        file = context.buffer
    if context.first:
        context.first = False
        text = sep.join(objects)
    else:
        text = context.indent + sep.join(objects)
    log_debug('PRINT\n{}\nPRINTEND'.format(text))
    builtins.print(text, end=end, file=file)

def execute(code, indent, first):
    buffer = io.StringIO()
    indent = re.sub(r'[^\t]', ' ', indent)
    code = re.sub(r'^{}'.format(indent), '', code, flags=re.MULTILINE)
    log_debug('INDENT{}INDENTEND'.format(indent))
    log_debug('CODE\n{}\nCODEEND'.format(code))
    prev_buffer = context.buffer
    prev_indent = context.indent
    prev_first = context.first
    context.buffer = buffer
    context.indent = indent
    context.first = first
    exec(code, globals(), context.variables)
    context.buffer = prev_buffer
    context.indent = prev_indent
    context.first = prev_first
    output = buffer.getvalue()
    if output.endswith('\n'):
        output = output[:-1]
    log_debug('OUTPUT\n{}\nOUTPUTEND'.format(output))
    return output

def match_with_indent_and_escape(match):
    matches = list(match.groups())
    indent = matches[0]
    i = 1
    code = ''
    while i < len(matches):
        if matches[i] != None:
            code += matches[i]
        i += 1
    code = re.sub(r' ?\\\n', '\n', code, flags=re.MULTILINE)
    return execute(code, indent, False)

def match_without_indent(match):
    prefix = match.string[0:match.start()]
    indent = prefix[prefix.rfind('\n') + 1:]
    code = ''
    for m in list(match.groups()):
        if m != None:
            code += m
    return execute(code, indent, True)

def srcdir():
    return context.srcdir

def dstdir():
    return context.dstdir

def expand(input='', output='', type='auto', newline=None):
    log_debug('expand input={} output={}'.format(input, output))
    cwd = os.getcwd()
    prev_srcdir = context.srcdir
    prev_dstdir = context.dstdir
    if newline == None:
        newline = os.linesep
    if output != '':
        context.dstdir = os.path.dirname(output)
        if not os.path.exists(context.dstdir):
            os.makedirs(context.dstdir)
    if input == '':
        src = sys.stdin
    else:
        src = open(input, 'rt')
        context.srcdir = os.path.dirname(input)
        dirname = os.path.dirname(input)
        if dirname != '':
            os.chdir(dirname)
    new_contents = src.read()
    regexes = [
        (re.compile(r'^([ \t]*)#[ \t]*py[ \t]*((?:.*\\\n)*.*)$', re.MULTILINE), match_with_indent_and_escape),
        (re.compile(r'<!--[ \t]*py[ \t]*(.*?)[ \t]*-->', re.DOTALL | re.MULTILINE), match_without_indent),
        (re.compile(r'/\*\**[ \t]*py[ \t]*(.*?)[ \t]*\*/', re.DOTALL | re.MULTILINE), match_without_indent),
        (re.compile(r'//[ \t]*py[ \t]*(.*)$', re.MULTILINE), match_without_indent)
    ]
    for regex, match_function in regexes:
        new_contents = regex.sub(match_function, new_contents)
    if input != '':
        src.close()
    result = True
    os.chdir(cwd)
    if output == '':
        if context.buffer != None:
            context.buffer.write(new_contents)
        else:
            sys.stdout.write(new_contents)
    else:
        old_contents = ''
        exists = os.path.exists(output)
        if exists:
            with open(output, 'rt', newline=newline) as fp:
                old_contents = fp.read()
        if not exists or old_contents != new_contents:
            with open(output, 'wt', newline=newline) as fp:
                fp.write(new_contents)
        else:
            result = False
    context.srcdir = prev_srcdir
    context.dstdir = prev_dstdir
    return result

def main():
    options = parse_options()
    context.debug = options.debug
    expand(options.input, options.output, options.type)

if __name__ == '__main__':
    main()
